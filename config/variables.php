<?php

return [
    
    'boolean' => [
        '0' => 'No',
        '1' => 'Yes',
    ],

    'role' => [
        '0' => 'User',
        '1' => 'Patient',
        '2' => 'Doctor',
        '10' => 'Admin',
    ],
    
    'status' => [
        '1' => 'Active',
        '0' => 'Inactive',
    ],

    'region' => [
        'ncr' => 'Delhi NCR',
    ],

    'queue_status' => [
        '0' => 'Scheduled',
        '1' => 'In Queue',
        '2' => 'Onging',
        '3' => 'Completed',
        '4' => 'Inactive',
    ],

    'speciality' => [
        'general' => 'General Physician',
        'ortho' => 'Orthopedic',
        'gyna' => 'Gynacologist',
        'cardio' => 'Cardiologists',
        'surgeons' => 'Surgeons',
    ],

    'avatar' => [
        'public' => '/storage/avatar/',
        'folder' => 'avatar',
        
        'width'  => 400,
        'height' => 400,
    ],

    /*
    |------------------------------------------------------------------------------------
    | ENV of APP
    |------------------------------------------------------------------------------------
    */
    'APP_ADMIN' => 'admin',
    'APP_TOKEN' => env('APP_TOKEN', 'admin123456'),
];
