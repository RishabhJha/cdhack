<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use App\Patient;
use App\User;
use App\Appointmemt;
use Auth;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware(['auth', 'Role:1'], ['except' => ['create', 'store']]);
    }

    public function index()
    {
        $doctors = Doctor::where('region', '=', Auth::user()->info->region)->get();

        return view('doctor.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('doctor.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, User::rules());
        
        User::create($request->all());

        return back()->withSuccess(trans('app.success_store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doctor = Doctor::find($id);
        return view('doctor.details', compact('doctor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function appointment($id)
   {
       $doctor = Doctor::find($id);
       $check = Appointmemt::where('doctor', '=', $id)
                            ->where('paitent', '=', Auth::id())
                            ->whereRaw("date(created_at) = '".date('Y-m-d')."'");
       if($check->get()->count() > 0){
          $appointment = $check->first();
       }else{
        $appointment = new Appointmemt;
        $appointment->doctor = $id;
        $appointment->paitent = Auth::id();
        $appointment->status = 0;
        $appointment->save();
       }
       
        $id = $appointment->id;
        $all_appointments = Appointmemt::whereRaw("date(created_at) = '".date('Y-m-d')."'")->orderBy('id', 'ASC')->first();
       $pos = $all_appointments->id - $id;
       $appointment = Appointmemt::find($id);

       return view("doctor.appointment", compact("appointment", "doctor", "pos"));
   }

    // public function setAppointment(Request $request)
    // {
    //     $appointment = new Appointmemt;
    //     $appointment->patient = Auth::id();
    //     $appointment->doctor = $request->id;
    //     $appointment->status = 0;
    //     $id = $appointment->save();

    //     return back()->withSuccess('Appointment create with appointment ID: $id');
    // }
}
