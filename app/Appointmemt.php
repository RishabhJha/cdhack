<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointmemt extends Model
{
  public function doctor()
  {
      return $this->hasOne(‘App\Doctor’, 'doctor');
  }

  public function patient()
  {
      return $this->hasOne(‘App\Patient’, 'paitent');
  }
}
