@extends('layouts.base')

@section('content')

@include('layouts.nav')

<div id="login-overlay" class="modal-dialog">
      <div class="modal-content text-center">
          <div class="modal-header">
              <h4 class="modal-title text-success" id="myModalLabel">Appointment Booked with Appointment ID-{{$appointment['id']}} !!</h4>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-sm-12">
                      <div class="well">
                        <!-- <div class="row"> -->
                          <!-- <div class="col-sm-6"> -->
                            <div>
                              You Have an Appointment with {{$doctor['name']}}.
                            </div>
                            <div>
                              You are number {{$pos}} in the queue.
                              <div class="text-danger" style="font-size:50px;">
                                {{$appointment['id']}}
                              </div>.
                            </div>
                          <!-- </div> -->
                        <!-- </div> -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


@endsection