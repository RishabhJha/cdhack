<nav class="navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="#">
    <img src="/images/first-aid-logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
    MEDICUS
  </a>
  <ul class="navbar-nav mr-2">
    <li class="nav-item active mr-4">
      <a class="nav-link" href="/patient/{{Auth::id()}}" style="text-transform: uppercase;">{{Auth::user()->name}}<span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('logout')}}">Logout</a>
    </li>
  </ul>
</nav>
