<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="/images/first-aid-logo.png"/>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Medicus</title>

  <!-- Styles -->
  <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
</head>
<body class="app">

    @include('admin.partials.spinner')

    <div class="peers ai-s fxw-nw h-100vh">
      <div class="d-n@sm- peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv" style='background-image: url("/images/angle-bg.png");background-image:linear-gradient(150deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);'>
        <div class="pos-a centerXY">
          <div class=" bdrs-50p pos-r" style='width: 120px; height: 120px;'>
            <img class="pos-a centerXY" src="/images/first-aid-logo.png" alt="">
            <!-- <span style="height:70px;width:70px;">
              <i class="fa fa-user-md" aria-hidden="true"></i>
              <svg></svg> -->
            <!-- </span> -->
          </div>
          <span class="text-white text-center" style="font-size:26px;font-weight:700;">MEDICUS</span>
        </div>
      </div>
      <div class="col-12 col-md-4 peer pX-40 pY-80 h-100 bgc-white scrollable pos-r" style='min-width: 320px;'>
        @yield('content')
      </div>
    </div>
  
</body>
</html>
