<?php

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::group(['prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware'=>['auth', 'Role:10']], function () {
    Route::get('/', 'DashboardController@index')->name('dash');
    Route::resource('users', 'UserController');
});

Route::get('/', 'DoctorController@index');

Route::get('/appointment/{id}', 'DoctorController@appointment');
// Route::post('/appointment', 'DoctorController@setAppointment');
Route::resource('doctor', 'DoctorController');
Route::resource('patient', 'PatientController');
