<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmemtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointmemts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('paitent')->unsigned();
            $table->bigInteger('doctor')->unsigned();
            $table->string('status');
            $table->text('doctor_remarks')->nullable();
            $table->text('patient_remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointmemts');
    }
}
