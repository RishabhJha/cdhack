<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('user_id')->unsigned();
            $table->date('dob')->nullable();
            $table->string('gender')->nullable();
            $table->string('contact')->nullable();
            $table->string('address')->nullable();
            $table->string('speciality')->nullable();
            $table->string('experience')->nullable();
            $table->string('region')->nullable();
            $table->string('status')->nullable();
            $table->text('avail')->nullable();
            $table->text('other_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
